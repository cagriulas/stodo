import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import ToggleButton from 'react-bootstrap/ToggleButton'
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup'
import Modal from 'react-bootstrap/Modal'
import ListGroup from 'react-bootstrap/ListGroup'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Formik } from 'formik'
import * as yup from 'yup'
import ModalDialog from 'react-bootstrap/ModalDialog';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faTrash, faCheckSquare } from '@fortawesome/free-solid-svg-icons'

const validationSchema = yup.object({
    name: yup.string().max(30).required("Name is a required field"),
    desc: yup.string().max(50).required("Description is a required field"),
});

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            todos: [],
            filterType: 1,
            selected_todo: {
                id: 0,
                name: 'Select a todo for viewing its description',
                desc: '',
                is_completed: false,
            },
            isFormVisible: false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleToggleForm = this.handleToggleForm.bind(this);
    };

    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    changeListedTodos(filter) {
        this.setState(state => {
            return {
                filterType: filter
            }
        })
    }

    componentDidMount() {
        fetch("http://127.0.0.1:8000/api/v1/todos")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        todos: result
                    });
                },
                (error) => {
                    this.setState({});
                }
            )
    }

    handleValidation() {
        let valid = true
        const errors = []
        console.log(this.state)
        if (this.state.desc.length > 50) {
            errors.push("Description must contain less than 50 characters.")
            valid = false
        }
        this.setState(state => {
            return {
                errorDesc: errors
            }
        })
        return valid
    }

    handleSubmit(values) {
        fetch('http://127.0.0.1:8000/api/v1/todos/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: values.name,
                desc: values.desc,
                is_completed: false
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState(state => {
                        const todos = [...state.todos, result];
                        return {
                            todos,
                            isFormVisible: !this.state.isFormVisible
                        };
                    });
                },
                (error) => {
                    this.setState({});
                }
            )
    }

    handleToggleForm() {
        this.setState({
            isFormVisible: !this.state.isFormVisible
        });
    }

    handleSelectTodo(clicked_todo) {
        this.setState({
            selected_todo: clicked_todo
        })

        fetch(`http://127.0.0.1:8000/api/v1/todos/${clicked_todo.id}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState(state => {
                        return {
                            selected_todo: result
                        };
                    });
                },
                (error) => {
                    this.setState({});
                }
            )
    }

    markAsDoneTodo(clicked_todo) {
        console.log(clicked_todo)
        fetch(`http://127.0.0.1:8000/api/v1/todos/${clicked_todo.id}/`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: clicked_todo.name,
                desc: clicked_todo.desc,
                is_completed: clicked_todo.is_completed ? "false" : "true"
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState(state => {
                        const todos = state.todos.map((todo, j) => {
                            if (todo.id === clicked_todo.id) {
                                if (todo.is_completed) {
                                    todo.is_completed = false
                                } else {
                                    todo.is_completed = true
                                }
                                return todo;
                            } else {
                                return todo;
                            }
                        });
                        const selected_todo = state.selected_todo
                        selected_todo["is_completed"] = !selected_todo["is_completed"]
                        return {
                            todos,
                            name: state.name,
                            desc: state.desc,
                            selected_todo: selected_todo
                        };
                    });
                },
                (error) => {
                    this.setState({});
                }
            )
    }

    deleteTodo(clicked_todo) {
        fetch(`http://127.0.0.1:8000/api/v1/todos/${clicked_todo.id}/`, {
            method: 'DELETE'
        })
            .then(
                (result) => {
                    this.setState(state => {
                        const todos = state.todos.filter(todo => clicked_todo.id !== todo.id);
                        return {
                            todos,
                            selected_todo: {
                                id: 0,
                                name: 'Select a todo for viewing its description',
                                desc: '',
                                is_completed: false,
                            },
                        };
                    });
                },
                (error) => {
                    this.setState({});
                }
            )
    }

    render() {
        return (
            <Container fluid={true}>
                <Modal show={this.state.isFormVisible} onHide={this.handleToggleForm}>
                    <Formik
                        validationSchema={validationSchema}
                        onSubmit={values => {
                            console.log(values)
                            this.handleSubmit(values)
                        }}
                        initialValues={{
                            name: "",
                            desc: ""
                        }}
                    >
                        {({
                            handleSubmit,
                            handleChange,
                            handleBlur,
                            values,
                            touched,
                            errors,
                        }) => (
                                <Form noValidate onSubmit={handleSubmit}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Add New Todo</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <Form.Group controlId="name">
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                name="name"
                                                placeholder="Name"
                                                value={values.name}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                isValid={touched.name && !errors.name}
                                                isInvalid={touched.name && errors.name}
                                            />
                                            <Form.Control.Feedback type="invalid">
                                                {errors.name}
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Group controlId="desc">
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control
                                                as="textarea"
                                                rows="3"
                                                value={values.desc}
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                isValid={touched.desc && !errors.desc}
                                                isInvalid={touched.desc && errors.desc}
                                            />
                                            <Form.Control.Feedback type="invalid">
                                                {errors.desc}
                                            </Form.Control.Feedback>
                                        </Form.Group>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="primary" type="submit">Add</Button>
                                    </Modal.Footer>
                                </Form>
                            )}
                    </Formik>
                </Modal>
                <Row>
                    <Col>
                        <Modal.Dialog className="fix-modal-width" scrollable={true} style={{marginLeft: "0px !important", marginRight: "0px !important"}}>
                            <Modal.Header style={{display: "block"}}>
                                <Modal.Title>
                                    <Container>
                                        <Row>
                                            <Col style={{padding: "0px !important"}}>Todos</Col>
                                            
                                        </Row>
                                        <hr></hr>
                                        <Row>
                                            <Col>
                                                <ToggleButtonGroup type="radio" name="filterType" defaultValue={1} onChange={(e) => this.changeListedTodos(e)}>
                                                    <ToggleButton value={1}>All</ToggleButton>
                                                    <ToggleButton value={2}>Checked</ToggleButton>
                                                    <ToggleButton value={3}>Unchecked</ToggleButton>
                                                </ToggleButtonGroup>
                                            </Col>
                                            <Col md="2">
                                                <Button onClick={this.handleToggleForm}>
                                                    <FontAwesomeIcon icon={faPlus} />
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Container>
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <ListGroup variant="flush">
                                    {this.state.todos.filter(todo => {
                                        if ((todo.is_completed && (this.state.filterType === 1 || this.state.filterType === 2)) || 
                                            (!todo.is_completed && (this.state.filterType === 1 || this.state.filterType === 3))) {
                                            return todo
                                        }
                                    }).map((todo, index) => (
                                        <ListGroup.Item
                                            action={true}
                                            eventKey={todo.id}
                                            onClick={(e) => this.handleSelectTodo(todo)}
                                            variant={todo.is_complete ? "danger" : "alert"}
                                        >
                                            {todo.is_completed ?
                                                <div><strike>{todo.name}</strike></div> :
                                                <div>{todo.name}</div>}
                                        </ListGroup.Item>
                                    ))}
                                </ListGroup>
                            </Modal.Body>
                        </Modal.Dialog>
                    </Col>
                    <Col>
                        <ModalDialog>
                            <Modal.Header>
                                <Modal.Title>
                                    {this.state.selected_todo.name}
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <ListGroup>
                                    {this.state.selected_todo.desc}
                                </ListGroup>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button
                                    variant="danger"
                                    disabled={this.state.selected_todo.id === 0 ? true : false}
                                    onClick={(e) => this.deleteTodo(this.state.selected_todo)}
                                >
                                    <FontAwesomeIcon icon={faTrash} />
                                </Button>
                                <Button
                                    variant={
                                        this.state.selected_todo.is_completed ?
                                            "primary" :
                                            "secondary"
                                    }
                                    disabled={
                                        this.state.selected_todo.id === 0 ?
                                            true :
                                            false
                                    }
                                    value="0"
                                    onClick={(e) => this.markAsDoneTodo(this.state.selected_todo)}
                                >
                                    <FontAwesomeIcon icon={faCheckSquare} />
                                </Button>
                            </Modal.Footer>
                        </ModalDialog>
                    </Col>
                </Row>
            </Container>
        )
    }
}


ReactDOM.render(
    <App />,
    document.getElementById('root')
);

