from .models import ToDo
from rest_framework import viewsets, permissions
from .serializers import ToDoSerializer


class ToDoViewSet(viewsets.ModelViewSet):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer
    permission_classes = [permissions.AllowAny]

