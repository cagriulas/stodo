from rest_framework import serializers
from .models import ToDo


class ToDoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ToDo
        fields = ('id', 'name', 'desc', 'is_completed', 'created_at', 'last_edited_at')
