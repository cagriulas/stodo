from django.db import models


class ToDo(models.Model):
    name = models.CharField(max_length=30)
    desc = models.CharField(max_length=50)
    is_completed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    last_edited_at = models.DateTimeField(auto_now=True)
