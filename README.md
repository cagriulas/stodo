# STodo
Simple todo application with django rest framework and react

## required tools and programs
- yarn (nodejs)
- poetry (python)
- postgresql

## Build

```bash
$ poetry shell
$ poetry update
$ cd stodo
$ yarn
```

## Run
for ui
```bash
$ cd stodo
$ yarn start
```

for backend

```bash
$ sudo su - postgres
$ psql
postgres=# create database stodo;
postgres=# create user stodo with password 'stodo';
postgres=# alter role stodo set client_encoding to 'utf8';
postgres=# alter role stodo SET default_transaction_isolation TO 'read committed';
postgres=# alter role stodo SET timezone TO 'UTC';
postgres=# GRANT ALL PRIVILEGES ON DATABASE stodo to stodo;
```

```bash
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py runserver
```

## Road Map

- ~~filter todos according to is_completed field~~
- ~~more error handling on ui~~
- serve static file with django
- ~~make ui great again with bootstrap~~
- change file structure with better one
- add users with todo relations
